use std::collections::HashMap;

struct Solution {}

// Definition for singly-linked list.
#[derive(PartialEq, Eq, Clone, Debug)]
pub struct ListNode {
    pub val: i32,
    pub next: Option<Box<ListNode>>,
}

impl ListNode {
    #[inline]
    fn new(val: i32) -> Self {
        ListNode { next: None, val }
    }
}

impl Solution {
    // pub fn two_sums(nums: Vec<i32>, target: i32) -> Vec<i32> {
    //     for i in 0..nums.len() {
    //         for j in 0..nums.len() {
    //             if i != j {
    //                 if nums[i] + nums[j] == target {
    //                     return Vec::from([i as i32, j as i32]);
    //                 }
    //             }
    //         }
    //     }
    //     return Vec::from([0, 0]);
    // }

    // pub fn is_palindrome(x: i32) -> bool {
    //     let s = x.to_string();
    //     for i in 0..s.len() / 2 {
    //         println!("{}", &s[i..i + 1]);
    //         let j = s.len() - i;
    //         println!("{}", &s[j - 1..j]);
    //         if &s[i..i + 1] != &s[j - 1..j] {
    //             return false;
    //         }
    //     }
    //     true
    // }

    // pub fn roman_to_int(s: String) -> i32 {

    //     fn calc(chars: Vec<char>, ichar: usize, result: i32) -> (i32, i32) {
    //         if(ichar < chars.len()) {

    //             if(chars[ichar] == 'I') {
    //                 result += calc(chars, ichar + 1, result);
    //             }

    //         } else {
    //             return (0, 0);
    //         }

    //         (0, 0)
    //     }

    //     let chars : Vec<char> = s.chars().collect();

    //     let result = calc(chars, 0, 0);

    //     result.0
    // }

    // pub fn longest_common_prefix(strs: Vec<String>) -> String {
    //     let mut result = String::new();

    //     for i in 0..200 {
    //         let first_string_char = strs[0].chars().nth(i);

    //         if !first_string_char.is_some() {
    //             break;
    //         }

    //         let mut found = true;
    //         for str in &strs {
    //             if first_string_char != str.chars().nth(i) {
    //                 found = false;
    //                 break;
    //             }
    //         }
    //         if found {
    //             result.push(first_string_char.unwrap());
    //         } else {
    //             break;
    //         }
    //     }
    //     return result;
    // }

    // pub fn is_valid(s: String) -> bool {
    //     pub fn match_paren(c: char) -> char {
    //         match c {
    //             ')' => '(',
    //             ']' => '[',
    //             '}' => '{',
    //             _ => '0',
    //         }
    //     }

    //     pub fn is_opening(c: char) -> bool {
    //         match c {
    //             '(' | '[' | '{' => true,
    //             _ => false,
    //         }
    //     }

    //     if s.len() % 2 != 0 {
    //         return false;
    //     }
    //     let mut stack = Vec::new();
    //     for c in s.chars() {
    //         if is_opening(c) {
    //             stack.push(c);
    //         } else {
    //             let paren = match_paren(c);
    //             if stack.last().is_some() && paren == *stack.last().unwrap() {
    //                 stack.pop();
    //             } else {
    //                 return false;
    //             }
    //         }
    //     }
    //     if stack.len() == 0 {
    //         true
    //     } else {
    //         false
    //     }
    // }

    // pub fn remove_duplicates(nums: &mut Vec<i32>) -> i32 {
    //     fn rew(pos: usize, nums: &mut Vec<i32>, len: usize) -> usize {
    //         if nums[pos] > -1000 {
    //             for i in pos..len - 1 {
    //                 nums[i] = nums[i + 1];
    //                 nums[i + 1] = -1000;
    //             }
    //             return len - 1;
    //         } else {
    //             return len;
    //         }
    //     }

    //     let mut len = nums.len();
    //     for i in 0..len - 1 {
    //         if nums[i] > -1000 {
    //             while nums[i] == nums[i + 1] {
    //                 len = rew(i, nums, len);
    //             }
    //         }
    //     }
    //     return len as i32;
    // }

    // pub fn remove_element(nums: &mut Vec<i32>, val: i32) -> i32 {
    //     if nums.len() == 0 {
    //         return 0;
    //     }
    //     fn rew(pos: usize, nums: &mut Vec<i32>, len: usize) -> usize {
    //         if nums[pos] > -1000 {
    //             for i in pos..len - 1 {
    //                 nums[i] = nums[i + 1];
    //                 nums[i + 1] = -1000;
    //             }
    //             return len - 1;
    //         } else {
    //             return len;
    //         }
    //     }

    //     let mut len = nums.len();

    //     let mut i = 0;
    //     loop {
    //         if nums[i] == val {
    //             len = rew(i, nums, len);
    //         } else {
    //             i = i + 1;
    //             if i == len {
    //                 break;
    //             }
    //         }
    //     }
    //     return len as i32;
    // }

    // pub fn str_str(haystack: String, needle: String) -> i32 {
    //     if needle.len() > haystack.len() {
    //         return -1;
    //     }
    //     for i in 0..haystack.len() - needle.len() + 1 {
    //     let mut found = true;
    //         for (j, c) in needle.chars().enumerate() {
    //             if c != haystack.chars().nth(i + j).unwrap() {
    //                 found = false;
    //                 break;
    //             }
    //         }
    //         if found {
    //             return i as i32;
    //         }

    //     }
    //     -1
    // }

    // pub fn merge_two_lists(list1: Option<Box<ListNode>>, list2: Option<Box<ListNode>>) -> Option<Box<ListNode>> {
    // }

    // pub fn merge(nums1: &mut Vec<i32>, m: i32, nums2: &mut Vec<i32>, n: i32) {
    //     fn find_smallest(nums: &mut Vec<i32>, i1: i32, m: i32) -> i32 {
    //         let mut result = i32::MAX;
    //         for i in i1..m {
    //             if nums[i as usize] < result {
    //                 result = nums[i as usize];
    //             }
    //         }
    //         result
    //     }

    //     let mut i1 = 0;
    //     let mut i2 = 0;
    //     let mut result: Vec<i32> = Vec::with_capacity((m + n) as usize);

    //     loop {
    //         if i1 < m && i2 < n {
    //             let s1 = find_smallest(nums1, i1, m);
    //             let s2 = find_smallest(nums2, i2, n);
    //             if s1 < s2 {
    //                 result.push(s1);
    //                 i1 += 1;
    //             } else {
    //                 result.push(s2);
    //                 i2 += 1;
    //             }
    //         } else if i1 < m {
    //             result.push(nums1[i1 as usize]);
    //             i1 += 1;
    //         } else if i2 < n {
    //             result.push(nums2[i2 as usize]);
    //             i2 += 1;
    //         } else {
    //             break;
    //         }
    //     }
    //     for (i, n) in result.iter().enumerate() {
    //         nums1[i] = *n;
    //     }
    // }

    pub fn majority_element(nums: Vec<i32>) -> i32 {
        let mut hits: HashMap<i32, i32> = HashMap::new();

        for n in &nums {
            let v = hits.get_mut(&n);
            if v.is_some() {
                *v.unwrap() += 1;
            } else {
                hits.insert(*n, 1);
            }
        }

        for h in &hits {
            if *h.1 > nums.len() as i32 / 2 {
                return *h.0;
            }
        }
        0
    }

    pub fn max_profit(prices: Vec<i32>) -> i32 {
        let mut max_profit = 0;

        for i in 0..prices.len() {
            for j in i..prices.len() {
                let profit = prices[j] - prices[i];
                if profit > max_profit {
                    max_profit = profit;
                }
            }
        }
        max_profit
    }

    pub fn length_of_last_word(s: String) -> i32 {
        let mut len = 0;
        for (i, c) in s.chars().rev().enumerate() {
            if c != ' ' {
                len += 1;
            } else if len > 0 {
                break;
            }
        }
        len
    }

    pub fn is_palindrome(s: String) -> bool {
        let c = 'J';
        println!("{:?}", c.to_ascii_lowercase());
        true
    }
}

fn main() {
    println!(
        "{}",
        Solution::is_palindrome("amanaplanacanalpanama".to_string())
    );
}
